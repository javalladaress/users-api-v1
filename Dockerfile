FROM mhart/alpine-node:12
COPY . /api/
WORKDIR /api
RUN npm install
EXPOSE 2001/tcp
CMD ["npm", "start"]
