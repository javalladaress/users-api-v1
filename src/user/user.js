export default function buildMakeUser ({ md5 }) {
  return function makeUser ({
    id,
    email,
    name,
    lastName,
    hash
  } = {}) {

    return Object.freeze({
      getId: () => id,
      getEmail: () => email,
      getName: () => name,
      getLastName: () => lastName,
      getHash: () => (hash = makeHash())
    })

    console.log(arguments);

    function makeHash () {
      return md5(id + email + name + lastName)
    }

  }
}
