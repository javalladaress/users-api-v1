import crypto from 'crypto'
import buildMakeUser from './user'

const makeUser = buildMakeUser({ md5 })

export default makeUser

function md5 (text) {
  return crypto
    .createHash('md5')
    .update(text, 'utf-8')
    .digest('hex')
}
