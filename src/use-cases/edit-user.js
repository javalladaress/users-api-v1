import makeUser from '../user'

export default function makeEditUser ({ usersDaaS }) {

  return async function editUser ({ id, userInfo } = {}) {
    if (!id) {
      throw new Error('You must supply an id.')
    }

    const existing = await usersDaaS.findById({ id })

    if (!existing) {
      throw new RangeError('User not found.')
    }

    const user = makeUser({ userInfo })
    
    if (user.getHash() === existing.hash) {
      console.log(comment)
      return existing
    }

    const updated = await usersDaaS.update({
      id: user.getId(),
      email: user.getEmail(),
      name: user.getName(),
      lastName: user.getLastName(),
      hash: user.getHash()
    })

    return { existing, updated }
  }
}
