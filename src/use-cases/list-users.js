export default function makeListUsers ({ usersDaaS }) {
  return async function listUsers ({} = {}) {
    const users = await usersDaaS.findAll({})
    return users
  }
}
