import { nanoid } from 'nanoid'
const { OAuth2Client } = require('google-auth-library');

const env = require('../environment');

const gClientId = env.getProperty('google.clientId');

async function verify(client, token) {
  const ticket = await client.verifyIdToken({
      idToken: token,
      audience: gClientId
  });
  
  const payload = ticket.getPayload();

  return {
    'name': payload['given_name'],
    'lastName': payload['family_name'],
    'domain': payload['hd'],
    'email': payload['email'],
    'verified': payload['email_verified'],
    'exp': payload['exp']
  };
}

export default function makeAuthenticateUser ({ usersDaaS, sessionsDaaS }) {
  return async function authenticateUser (email, gToken) {
    
    const client = new OAuth2Client(gClientId);

    const user = await verify(client, gToken);

    if( user['email'] != email ) { return null; }

    const _user = await usersDaaS.findByEmail(email);

    const userId = _user['id'];
    const sessionId = nanoid();

    user['id'] = userId;
    user['ssn'] = sessionId;

    (await sessionsDaaS.findByUserId(userId))
    .forEach(function(sessionEntity) {
      sessionsDaaS.remove(sessionEntity._id.$oid);
    });

    const issuedAt = Date.now();

    sessionsDaaS.insert({
      sid: sessionId,
      uid: userId,
      iat: issuedAt,
      exp: issuedAt + ( 30 * 60 * 1000 )
    });

    return user;
    
  }
}
