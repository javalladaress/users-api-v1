export default function makeGetUser ({ usersDaaS }) {
  return async function getUser ({ userId } = {}) {
    if (!userId) {
      throw new Error('You must supply a user id.')
    }
    const user = await usersDaaS.findById(userId)
    return user
  }
}
