export default function makeEnforceSession ({ sessionsDaaS }) {
  return async function enforceSession (userId, sessionId) {
    
    const timestamp = Date.now();

    const sessionEntity = await sessionsDaaS.find(userId, sessionId);

    if( sessionEntity == null ) {      
      return false;
    }

    const ageSeconds = (timestamp - sessionEntity.iat)/1000;
    const secondsToLive = (sessionEntity.exp - timestamp)/1000;

    if( (Math.floor(ageSeconds/60) >= 30) || (secondsToLive <= 0) ) {
      sessionsDaaS.remove(sessionEntity._id.$oid);
      return false;
    }

    return true;    
  }
}
