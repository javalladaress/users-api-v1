export default function makeTerminateSession ({ sessionsDaaS }) {
  return async function terminateSession (userId, sessionId) {

    const sessionEntity = await sessionsDaaS.find(userId, sessionId);

    if( sessionEntity == null ) {      
      return false;
    }

    const session = await sessionsDaaS.remove(sessionEntity._id.$oid);

    return session.sid == sessionEntity.sid;
    
  }
}
