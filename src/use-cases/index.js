import makeAddUser from './add-user'
import makeEditUser from './edit-user'
import makeRemoveUser from './remove-user'
import makeGetUser from './get-user'
import makeListUsers from './list-users'
import makeAuthenticateUser from './login-user'
import makeTerminateSession from './logout-user'
import makeEnforceSession from './enforce-session'
import {
  usersDaaS,
  sessionsDaaS
} from '../daas'

const findUser = makeGetUser({ usersDaaS })
const listUsers = makeListUsers({ usersDaaS })
const addUser = makeAddUser({ usersDaaS })
const editUser = makeEditUser({ usersDaaS })
const removeUser = makeRemoveUser({ usersDaaS })
const authenticateUser = makeAuthenticateUser({ usersDaaS, sessionsDaaS })
const terminateSession = makeTerminateSession({ sessionsDaaS })
const enforceSession = makeEnforceSession({ sessionsDaaS })

const usersService = Object.freeze({
  findUser,
  listUsers,
  addUser,
  editUser,
  removeUser,
  authenticateUser,
  terminateSession,
  enforceSession
})

export default usersService
export { findUser, listUsers, addUser, editUser, removeUser, authenticateUser, terminateSession, enforceSession }
