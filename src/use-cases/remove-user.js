export default function makeRemoveUser ({ usersDaaS }) {
  return async function removeUser ({ userId } = {}) {
    if (!userId) {
      throw new Error('You must supply a comment id.')
    }

    const userToDelete = await usersDaaS.findById(userId)

    if (!userToDelete) {
      return deleteNothing()
    }

    return deleteUser(userToDelete)
  }

  function deleteNothing () {
    return {
      deletedCount: 0,
      message: 'User not found, nothing to delete.'
    }
  }

  async function deleteUser (user) {
    await usersDaaS.remove(user._id.$oid)
    return {
      deletedCount: 1,
      'user': user,
      message: 'User deleted.'
    }
  }
}
