import makeUser from '../user'

export default function makeAddUser ({ usersDaaS }) {
  return async function addUser (userInfo) {

    const user = makeUser(userInfo)

    const exists = await usersDaaS.findByHash(user.getHash())
    
    if( exists ) {
      return exists
    }

    return usersDaaS.insert({
      id: user.getId(),
      email: user.getEmail(),
      name: user.getName(),
      lastName: user.getLastName(),
      hash: user.getHash()
    })
    
  }
}
