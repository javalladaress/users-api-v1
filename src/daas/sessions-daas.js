export default function makeSessionsDaaS ({ makeDaaS }) {
  return Object.freeze({
    find,
    findByUserId,
    insert,
    remove
  })

  async function findByUserId (userId) {
    const daas = makeDaaS()

    const result = await daas.get("/", {
      params: {
        q: JSON.stringify({
          'uid': userId
        })
      }
    })

    return result.data;
  }

  async function find (userId, sessionId) {
    const daas = makeDaaS()

    const result = await daas.get("/", {
      params: {
        q: JSON.stringify({
          'uid': userId,
          'sid': sessionId
        })
      }
    })

    let sessions = result.data;
    
    if (sessions.length === 0) {
      return null
    }

    const { ...info } = sessions[0]
    return { ...info }
  }

  async function insert (session) {
    const daas = makeDaaS()
    const result = await daas.post("/", session)
    const { _id: id, ...insertedInfo } = result.data
    return { id, ...insertedInfo }
  }

  async function remove (sessionEntityId) {
    const daas = makeDaaS()
    const result = await daas.delete("/" + sessionEntityId)
    return result.data
  }
}
