import makeUsersDaaS from './users-daas'
import makeSessionsDaaS from './sessions-daas'

const axios = require('axios').default;
const axiosRetry = require("axios-retry");

const env = require('../environment');

const usersCollection = env.getProperty('daas.collection.users');
const sessionsCollection = env.getProperty('daas.collection.sessions');

const setInterceptor = function(client) {
  client.interceptors.request.use((config) => {
    config.params = config.params || {};
    config.params['apiKey'] = env.getProperty('daas.key');
    return config;
  });
  return client;
}

export function makeDaaS (collection) {
  return function() {
    var client = setInterceptor(axios.create({
      baseURL: env.getProperty('daas.endpoint') + collection,
      params: {
        apiKey: env.getProperty('daas.key')
      }
    }))
    axiosRetry(client, {
      retries: 3,
      retryDelay: axiosRetry.exponentialDelay,
      shouldResetTimeout: true,
      retryCondition: (axiosError) => { return true; }
    })
    return client;
  }
}

const _usersDaaS = makeDaaS(usersCollection);
const _sessionsDaaS = makeDaaS(sessionsCollection);

const usersDaaS = makeUsersDaaS({ makeDaaS: _usersDaaS });
const sessionsDaaS = makeSessionsDaaS({ makeDaaS: _sessionsDaaS });

const daas = Object.freeze({
  usersDaaS,
  sessionsDaaS
})

export default daas
export { usersDaaS, sessionsDaaS }
