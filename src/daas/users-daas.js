export default function makeUsersDaaS ({ makeDaaS }) {
  return Object.freeze({
    findAll,
    findById,
    findByEmail,
    findByHash,
    insert,
    remove,
    update
  })
  async function findAll ({} = {}) {
    const daas = makeDaaS()
    const result = await daas.get("/")
    return (await result.data).map(({ _id: id, ...found }) => ({
      id,
      ...found
    }))
  }
  async function findById (userId) {
    const daas = makeDaaS()
    const result = await daas.get("/", {
      params: {
        q: JSON.stringify({'id': userId})
      }
    })
    let users = result.data;
    if (users.length === 0) {
      return null
    }
    const { ...info } = users[0]
    return { ...info }
  }
  async function findByEmail (userEmail) {
    const daas = makeDaaS()
    const result = await daas.get("/", {
      params: {
        q: JSON.stringify({ 'email': userEmail }),
        f: JSON.stringify({ 'id': 1 })
      }
    })
    let users = result.data;
    if (users.length === 0) {
      return null
    }
    const { _id: id, ...info } = users[0]
    return { id, ...info }
  }
  async function findByHash (hash) {
    const daas = makeDaaS()
    const result = await daas.get("/", {
      params: {
        q: JSON.stringify({'hash': hash})
      }
    })
    let users = result.data;
    if (users.length === 0) {
      return null
    }
    const { _id: id, ...info } = users[0]
    return { id, ...info }
  }
  async function insert (user) {
    const daas = makeDaaS()
    const result = await daas.post("/", user)
    const { _id: id, ...insertedInfo } = result.data[0]
    return { id, ...insertedInfo }
  }
  async function update ({ id: _id, ...commentInfo }) {
    const db = await makeDb()
    const result = await db
      .collection('comments')
      .updateOne({ _id }, { $set: { ...commentInfo } })
    return result.modifiedCount > 0 ? { id: _id, ...commentInfo } : null
  }
  async function remove (oid) {
    const daas = makeDaaS()
    const result = await daas.delete("/" + oid)
    return null
  }
}
