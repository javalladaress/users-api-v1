export default function makeValidateSession ({ enforceSession }) {
  return async function validateSession (httpRequest) {
    try {

      const sessionInfo = {
        uid: httpRequest.params.uid,
        sid: httpRequest.params.sid
      };

      const isValidSession = await enforceSession(sessionInfo.uid, sessionInfo.sid);

      sessionInfo['valid'] = isValidSession;

      return {
        headers: {
          'Content-Type': 'application/json',
        },
        statusCode: 200,
        body: sessionInfo
      }

    } catch (e) {
      console.log(e)
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 400,
        body: {
          error: e.message
        }
      }
    }
  }
}
