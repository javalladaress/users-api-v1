export default function makeUpdateUser ({ editUser }) {
  return async function updateUser (properties, httpRequest) {
    try {
      const user = httpRequest.body
      const toEdit = {
        user,
        id: httpRequest.params.id
      }
      const edited = await editUser(toEdit)
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 200,
        body: { edited }
      }
    } catch (e) {
      console.log(e)
      if (e.name === 'RangeError') {
        return {
          headers: {
            'Content-Type': 'application/json'
          },
          statusCode: 404,
          body: {
            error: e.message
          }
        }
      }
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 400,
        body: {
          error: e.message
        }
      }
    }
  }
}
