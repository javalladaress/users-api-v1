export default function makeCreateUser ({ addUser }) {
  return async function createUser (httpRequest) {
    try {
      const userInfo = httpRequest.body
      const user = await addUser(userInfo)
      return {
        headers: {
          'Content-Type': 'application/json',
        },
        statusCode: 201,
        body: user
      }
    } catch (e) {
      console.log(e)
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 400,
        body: {
          error: e.message
        }
      }
    }
  }
}
