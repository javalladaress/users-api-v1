export default function makeGetUser ({ findUser }) {
  return async function getUser (httpRequest) {
    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      const user = await findUser({
        userId: httpRequest.params.id
      })
      return {
        headers,
        statusCode: 200,
        body: user
      }
    } catch (e) {
      console.log(e)
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      }
    }
  }
}
