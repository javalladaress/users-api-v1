import {
  findUser, 
  listUsers, 
  addUser, 
  editUser, 
  removeUser,
  authenticateUser,
  terminateSession,
  enforceSession
} from '../use-cases'
import makeDeleteUser from './delete-user'
import makeGetUser from './get-user'
import makeGetUsers from './list-users'
import makeCreateUser from './create-user'
import makeUpdateUser from './update-user'
import makeLoginUser from './login-user'
import makeLogoutUser from './logout-user'
import makeValidateSession from './validate-session'
import notFound from './not-found'

const getUser = makeGetUser({ findUser })
const getUsers = makeGetUsers({ listUsers })
const createUser = makeCreateUser({ addUser })
const updateUser = makeUpdateUser({ editUser })
const deleteUser = makeDeleteUser({ removeUser })
const loginUser = makeLoginUser({ authenticateUser })
const logoutUser = makeLogoutUser({ terminateSession })
const validateSession = makeValidateSession({ enforceSession })

const usersController = Object.freeze({
  getUser,
  getUsers,
  createUser,
  updateUser,
  deleteUser,
  loginUser,
  notFound,
  validateSession
})

export default usersController
export { getUser, getUsers, createUser, updateUser, deleteUser, loginUser, logoutUser, validateSession, notFound }
