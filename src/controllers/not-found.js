export default async function notFound (httpRequest) {

  console.log("NOT_FOUND");

  return {
    headers: {
      'Content-Type': 'application/json'
    },
    body: { error: 'Not found.' },
    statusCode: 404
  }
}
