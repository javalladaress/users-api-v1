export default function makeLogoutUser ({ terminateSession }) {
  return async function logoutUser (httpRequest) {
    try {

      const sessionInfo = {
        uid: httpRequest.params.uid,
        sid: httpRequest.params.sid
      };

      const wasTerminated = await terminateSession(sessionInfo.uid, sessionInfo.sid);

      sessionInfo['logout'] = wasTerminated;

      return {
        headers: {
          'Content-Type': 'application/json',
        },
        statusCode: 200,
        body: sessionInfo
      }

    } catch (e) {

      console.log(e)
      
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 400,
        body: {
          error: e.message
        }
      }

    }

  }
}
