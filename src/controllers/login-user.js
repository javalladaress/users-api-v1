export default function makeLoginUser ({ authenticateUser }) {
  return async function loginUser (httpRequest) {
    try {

      const user = await authenticateUser(httpRequest.body.email, httpRequest.body.token);

      if( user == null ) {
        return {
          headers: {
            'Content-Type': 'application/json',
          },
          statusCode: 401,
          body: {
            error: "token invalido"
          }
        }
      }

      return {
        headers: {
          'Content-Type': 'application/json',
        },
        statusCode: 200,
        body: user
      }

    } catch (e) {
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 401
      }
    }
  }
}
