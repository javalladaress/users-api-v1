module.exports = function makeExpressCallback (controller) {
  return (req, res) => {

    const client = {
      uid: req.get('X-BBVA-UserId'),
      email: req.get('X-BBVA-UserEmail')
    };

    const sessionId = req.get('X-BBVA-SessionId');

    console.log(client, req.method, req.path);

    const httpRequest = {
      body: req.body,
      query: req.query,
      params: req.params,
      ip: req.ip,
      method: req.method,
      path: req.path,
      headers: {
        'Content-Type': req.get('Content-Type'),
        Referer: req.get('referer'),
        'User-Agent': req.get('User-Agent'),
        'X-BBVA-UserId': client['uid'],
        'X-BBVA-SessionId': sessionId,
        'X-BBVA-UserEmail': client['email']
      },
      'client': client,
      'session': sessionId,
      raw: req
    }
    controller(httpRequest)
      .then(httpResponse => {
        if (httpResponse.headers) {
          res.set(httpResponse.headers)
        }
        res.type('json')
        res.status(httpResponse.statusCode).send(httpResponse.body)
      })
      .catch(e => res.status(500).send({ error: 'An unkown error occurred.' }))
  }
}
