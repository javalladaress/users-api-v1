const express = require('express')
const cors = require('cors')
const compression = require('compression')
import bodyParser from 'body-parser'
import dotenv from 'dotenv'
import {
  getUser, 
  getUsers, 
  createUser, 
  updateUser, 
  deleteUser, 
  loginUser,
  logoutUser, 
  validateSession,
  notFound
} from './controllers'
import makeCallback from './express-callback'

dotenv.config()
const env = require('./environment');

const usersApiName = env.getProperty('api.users.name')
const usersApiPort = env.getProperty('api.users.port')

const sessionApiName = env.getProperty('api.session.name')
const sessionApiPort = env.getProperty('api.session.port')

const session = express()
const _logoutUser = makeCallback(logoutUser)
session.use(bodyParser.json())
session.post("/login", makeCallback(loginUser))
session.get("/login/:uid/:sid", makeCallback(validateSession))
session.get("/logout/:uid/:sid", _logoutUser)
session.delete("/logout/:uid/:sid", _logoutUser)

const users = express()
users.use(bodyParser.json())
users.get("/", makeCallback(getUsers))
users.post("/", makeCallback(createUser))
users.get(`/:id`, makeCallback(getUser))
users.put(`/:id`, makeCallback(updateUser))
users.delete(`/:id`, makeCallback(deleteUser))

const _notFount = makeCallback(notFound);
users.use(_notFount)
session.use(_notFount)

users.listen(usersApiPort, () => {
  console.log(usersApiName + ' is listening on port: ' + usersApiPort)
})

session.listen(sessionApiPort, () => {
  console.log(sessionApiName + ' is listening on port: ' + sessionApiPort)
})

export default users